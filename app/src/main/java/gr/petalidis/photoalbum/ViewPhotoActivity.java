package gr.petalidis.photoalbum;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import gr.petalidis.photoalbum.model.Photo;

public class ViewPhotoActivity extends AppCompatActivity {
    private static final String TAG = ViewPhotoActivity.class.getName();


    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null) {
                photo = (Photo) intent.getSerializableExtra("photo");
            }
        } else {
            photo = (Photo)savedInstanceState.get("photo");
        }
        setUpView();

        Log.d(TAG, "onCreate");

    }

    private void setUpView()
    {
        TextView textView = findViewById(R.id.nameTextView);
        textView.setText(photo.getName());
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bmp = BitmapFactory.decodeByteArray(photo.getData(), 0, photo.
                getData().length);
        imageView.setImageBitmap(bmp);
    }
    public void onTryAgainClick(View view) {
        Intent intent = new Intent(this, ViewAnimalsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.photo = (Photo) savedInstanceState.getSerializable("photo");
        Log.d(TAG, "onRestoreInstanceState");
    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("photo",this.photo);
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");

    }

}