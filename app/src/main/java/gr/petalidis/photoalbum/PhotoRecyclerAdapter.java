package gr.petalidis.photoalbum;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gr.petalidis.photoalbum.model.Photo;

class PhotoRecyclerAdapter extends
        RecyclerView.Adapter<PhotoViewHolder> {
    List<Photo> photoList;
    private Context context;

    public PhotoRecyclerAdapter(List<Photo> photoList) {
        this.photoList = photoList;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    @NonNull
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View photoView = inflater.inflate(R.layout.photo_recycler_item, parent, false);

        // Return a new holder instance
        return  new PhotoViewHolder(photoView);
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        // Get the data model based on position
        Photo photo = photoList.get(position);

        // Set item views based on your views and data model
        TextView textView = holder.photoDescription;
        textView.setText(photo.getName());

        textView.setOnClickListener(
                view -> {
                   // MainActivity mainActivity = (MainActivity)context;
                   // mainActivity.setLastChoice(photo.getName());
                    Intent intent = new Intent(context,ViewPhotoActivity.class);
                    intent.putExtra("photo",photo);
                    context.startActivity(intent);
                });
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return photoList.size();
    }

}

