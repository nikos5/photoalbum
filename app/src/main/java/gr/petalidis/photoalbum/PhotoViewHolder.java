package gr.petalidis.photoalbum;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class PhotoViewHolder extends RecyclerView.ViewHolder {
    TextView photoDescription;

    public PhotoViewHolder(View itemView) {
        // Stores the itemView in a public final member variable that can be used
        // to access the context from any ViewHolder instance.
        super(itemView);

        photoDescription = itemView.findViewById(R.id.photoRecyclerId);
    }
}
