package gr.petalidis.photoalbum;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import gr.petalidis.photoalbum.client.PhotoApiClient;
import gr.petalidis.photoalbum.model.Photo;

public class ViewAnimalsViewModel extends ViewModel {
   private MutableLiveData<List<Photo>> photos;
   private PhotoApiClient photoApiClient  = new PhotoApiClient();

   public LiveData<List<Photo>> getPhotos(String filterSelection) {
      if (photos == null) {
         photos = new MutableLiveData<>();
      }
      loadPhotos(filterSelection);

      return photos;
   }

   private void loadPhotos(String filterSelection) {
      if (filterSelection.isEmpty()) {
         photos = photoApiClient.getAllPhotos();
      } else
      {
         photos = photoApiClient.getFilteredPhotos(filterSelection);
      }
   }

}

