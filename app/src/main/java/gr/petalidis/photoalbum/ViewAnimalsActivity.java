package gr.petalidis.photoalbum;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ViewAnimalsActivity extends AppCompatActivity {

    private static final String TAG = ViewAnimalsActivity.class.getName();
    private ViewAnimalsViewModel model;
    private String filterSelection = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_animals);
        RecyclerView recyclerView = findViewById(R.id.photoRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        model = new ViewModelProvider(this).get(ViewAnimalsViewModel.class);
        if (savedInstanceState==null) {
            Intent intent = getIntent();
            filterSelection = intent.getStringExtra("filter");
        } else {
            filterSelection = savedInstanceState.getString("filter");
        }

        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.getPhotos(filterSelection).observe(this, photos -> {
            PhotoRecyclerAdapter photoRecyclerAdapter = new PhotoRecyclerAdapter(
                    photos);
            RecyclerView recyclerView = findViewById(R.id.photoRecycler);
            recyclerView.setAdapter(photoRecyclerAdapter);
        });
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }
    public void onCreateButton(View view) {
        Intent intent = new Intent(this,CreateActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.filterSelection =  savedInstanceState.getString("filter");
        Log.d(TAG, "onRestoreInstanceState");
    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("filter",this.filterSelection);
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");

    }
}